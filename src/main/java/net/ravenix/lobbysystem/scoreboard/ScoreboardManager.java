package net.ravenix.lobbysystem.scoreboard;

import com.google.common.collect.Lists;
import net.minecraft.server.v1_8_R3.*;
import net.ravenix.clansystem.bukkit.BukkitClan;
import net.ravenix.clansystem.bukkit.clan.ClanProvider;
import net.ravenix.clansystem.shared.clan.Clan;
import net.ravenix.clansystem.shared.clan.ClanMember;
import net.ravenix.clansystem.shared.clan.IClanProvider;
import net.ravenix.coinsystem.bukkit.BukkitCoins;
import net.ravenix.coinsystem.shared.coins.ICoinProvider;
import net.ravenix.coinsystem.shared.user.ICoinUser;
import net.ravenix.core.bukkit.BukkitCore;
import net.ravenix.core.shared.permission.IPermissionProvider;
import net.ravenix.core.shared.permission.user.IPermissionUser;
import net.ravenix.core.shared.player.ICorePlayer;
import net.ravenix.core.shared.player.ICorePlayerProvider;
import net.ravenix.core.shared.skin.ISkin;
import net.ravenix.core.shared.skin.ISkinProvider;
import net.ravenix.friendsystem.bukkit.BukkitFriends;
import net.ravenix.friendsystem.shared.friend.IFriendProvider;
import net.ravenix.playtime.bukkit.BukkitPlaytime;
import net.ravenix.playtime.shared.playtime.IPlaytimeProvider;
import net.ravenix.playtime.shared.user.IPlaytimeUser;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

public final class ScoreboardManager {

    public void applyScoreboard(Player player) {
        net.minecraft.server.v1_8_R3.Scoreboard scoreboard = new net.minecraft.server.v1_8_R3.Scoreboard();

        ScoreboardObjective scoreboardObjective = scoreboard.registerObjective("aaa", IScoreboardCriteria.b);

        scoreboardObjective.setDisplayName("§b§lR§3§lavenix §8● §eLobby");

        scoreboard.getPlayerScoreForObjective("", scoreboardObjective);

        PacketPlayOutScoreboardObjective createpacket = new PacketPlayOutScoreboardObjective(scoreboardObjective, 0);
        PacketPlayOutScoreboardDisplayObjective display = new PacketPlayOutScoreboardDisplayObjective(1, scoreboardObjective);

        ScoreboardScore scoreboardScore2;
        ScoreboardScore scoreboardScore3;

        ScoreboardScore scoreboardScore5;
        ScoreboardScore scoreboardScore6;

        ScoreboardScore scoreboardScore8;
        ScoreboardScore scoreboardScore9;

        ScoreboardScore scoreboardScore11;
        ScoreboardScore scoreboardScore12;

        ScoreboardScore scoreboardScore14;
        ScoreboardScore scoreboardScore15;

        IPermissionProvider permissionProvider = BukkitCore.getInstance().getPermissionProvider();
        final IPermissionUser permissionUser = permissionProvider.getPermissionUser(player.getUniqueId());

        scoreboardScore2 = new ScoreboardScore(scoreboard, scoreboardObjective, "§7Rang");
        scoreboardScore3 = new ScoreboardScore(scoreboard, scoreboardObjective, "§8» " + permissionUser.getHighestGroup().getColoredName());

        ICoinProvider coinProvider = BukkitCoins.getInstance().getCoinProvider();
        ICoinUser coinUser = coinProvider.getCoinUser(player.getUniqueId());

        scoreboardScore5 = new ScoreboardScore(scoreboard, scoreboardObjective, "§7Coins");
        scoreboardScore6 = new ScoreboardScore(scoreboard, scoreboardObjective, "§8» §6" + coinUser.getCoinsAsDecimal());

        IClanProvider clanProvider = BukkitClan.getInstance().getClanProvider();
        Clan clan = clanProvider.getClan(player.getUniqueId());

        scoreboardScore8 = new ScoreboardScore(scoreboard, scoreboardObjective, "§7Clan");
        if (clan != null) {
            scoreboardScore9 = new ScoreboardScore(scoreboard, scoreboardObjective, "§8» " + clan.display());
        } else {
            scoreboardScore9 = new ScoreboardScore(scoreboard, scoreboardObjective, "§8» §cKein Clan");
        }

        IPlaytimeProvider playtimeProvider = BukkitPlaytime.getInstance().getPlaytimeProvider();
        IPlaytimeUser playtimeUser = playtimeProvider.getPlaytimeUser(player.getUniqueId());

        scoreboardScore11 = new ScoreboardScore(scoreboard, scoreboardObjective, "§7Spielzeit");
        scoreboardScore12 = new ScoreboardScore(scoreboard, scoreboardObjective, "§8» §b" + playtimeUser.getPlaytimeFormat(true));

        IFriendProvider ifriendProvider = BukkitFriends.getInstance().getFriendProvider();
        ICorePlayerProvider corePlayerProvider = BukkitCore.getInstance().getCorePlayerProvider();
        List<UUID> onlineFriends = Lists.newArrayList();

        List<UUID> friends = ifriendProvider.getFriends(player.getUniqueId());
        if (friends != null) {
            friends.forEach(friendUUID -> {
                ICorePlayer corePlayer = corePlayerProvider.getCorePlayer(friendUUID);
                if (corePlayer != null)
                    onlineFriends.add(friendUUID);
            });
        } else {
            friends = Lists.newArrayList();
        }

        scoreboardScore14 = new ScoreboardScore(scoreboard, scoreboardObjective, "§7Freunde");
        scoreboardScore15 = new ScoreboardScore(scoreboard, scoreboardObjective, "§8» §a" + onlineFriends.size() + "§8/§c" + friends.size());


        ScoreboardScore scoreboardScore1 = new ScoreboardScore(scoreboard, scoreboardObjective, "§a");
        ScoreboardScore scoreboardScore4 = new ScoreboardScore(scoreboard, scoreboardObjective, "§2");
        ScoreboardScore scoreboardScore7 = new ScoreboardScore(scoreboard, scoreboardObjective, "§b");
        ScoreboardScore scoreboardScore10 = new ScoreboardScore(scoreboard, scoreboardObjective, "§c");
        ScoreboardScore scoreboardScore13 = new ScoreboardScore(scoreboard, scoreboardObjective, "§3");


        scoreboardScore1.setScore(14);
        scoreboardScore2.setScore(13);
        scoreboardScore3.setScore(12);
        scoreboardScore4.setScore(11);
        scoreboardScore5.setScore(10);
        scoreboardScore6.setScore(9);
        scoreboardScore7.setScore(8);
        scoreboardScore8.setScore(7);
        scoreboardScore9.setScore(6);
        scoreboardScore10.setScore(5);
        scoreboardScore11.setScore(4);
        scoreboardScore12.setScore(3);
        scoreboardScore13.setScore(2);
        scoreboardScore14.setScore(1);
        scoreboardScore15.setScore(0);

        PacketPlayOutScoreboardObjective removepacket = new PacketPlayOutScoreboardObjective(scoreboardObjective, 1);
        PacketPlayOutScoreboardScore packetPlayOutScoreboardScore1 = new PacketPlayOutScoreboardScore(scoreboardScore1);
        PacketPlayOutScoreboardScore packetPlayOutScoreboardScore2 = new PacketPlayOutScoreboardScore(scoreboardScore2);
        PacketPlayOutScoreboardScore packetPlayOutScoreboardScore3 = new PacketPlayOutScoreboardScore(scoreboardScore3);
        PacketPlayOutScoreboardScore packetPlayOutScoreboardScore4 = new PacketPlayOutScoreboardScore(scoreboardScore4);
        PacketPlayOutScoreboardScore packetPlayOutScoreboardScore5 = new PacketPlayOutScoreboardScore(scoreboardScore5);
        PacketPlayOutScoreboardScore packetPlayOutScoreboardScore6 = new PacketPlayOutScoreboardScore(scoreboardScore6);
        PacketPlayOutScoreboardScore packetPlayOutScoreboardScore7 = new PacketPlayOutScoreboardScore(scoreboardScore7);
        PacketPlayOutScoreboardScore packetPlayOutScoreboardScore8 = new PacketPlayOutScoreboardScore(scoreboardScore8);
        PacketPlayOutScoreboardScore packetPlayOutScoreboardScore9 = new PacketPlayOutScoreboardScore(scoreboardScore9);
        PacketPlayOutScoreboardScore packetPlayOutScoreboardScore10 = new PacketPlayOutScoreboardScore(scoreboardScore10);
        PacketPlayOutScoreboardScore packetPlayOutScoreboardScore11 = new PacketPlayOutScoreboardScore(scoreboardScore11);
        PacketPlayOutScoreboardScore packetPlayOutScoreboardScore12 = new PacketPlayOutScoreboardScore(scoreboardScore12);
        PacketPlayOutScoreboardScore packetPlayOutScoreboardScore13 = new PacketPlayOutScoreboardScore(scoreboardScore13);
        PacketPlayOutScoreboardScore packetPlayOutScoreboardScore14 = new PacketPlayOutScoreboardScore(scoreboardScore14);
        PacketPlayOutScoreboardScore packetPlayOutScoreboardScore15 = new PacketPlayOutScoreboardScore(scoreboardScore15);

        sendPacket(removepacket, player);
        sendPacket(createpacket, player);
        sendPacket(display, player);

        sendPacket(packetPlayOutScoreboardScore1, player);
        sendPacket(packetPlayOutScoreboardScore2, player);
        sendPacket(packetPlayOutScoreboardScore3, player);
        sendPacket(packetPlayOutScoreboardScore4, player);
        sendPacket(packetPlayOutScoreboardScore5, player);
        sendPacket(packetPlayOutScoreboardScore6, player);
        sendPacket(packetPlayOutScoreboardScore7, player);
        sendPacket(packetPlayOutScoreboardScore8, player);
        sendPacket(packetPlayOutScoreboardScore9, player);
        sendPacket(packetPlayOutScoreboardScore10, player);
        sendPacket(packetPlayOutScoreboardScore11, player);
        sendPacket(packetPlayOutScoreboardScore12, player);
        sendPacket(packetPlayOutScoreboardScore13, player);
        sendPacket(packetPlayOutScoreboardScore14, player);
        sendPacket(packetPlayOutScoreboardScore15, player);
    }

    private void sendPacket(Packet packet, Player player) {
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
    }

}
