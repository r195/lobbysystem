package net.ravenix.lobbysystem;

import de.dytanic.cloudnet.driver.CloudNetDriver;
import lombok.Getter;
import net.ravenix.lobbysystem.listener.*;
import net.ravenix.lobbysystem.scoreboard.ScoreboardManager;
import net.ravenix.lobbysystem.utils.CloudServer;
import net.ravenix.lobbysystem.utils.Inventorys;
import net.ravenix.lobbysystem.utils.Locations;
import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.World;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public final class LobbySystem extends JavaPlugin {

    @Getter
    private static LobbySystem instance;
    @Getter
    private ScoreboardManager scoreboardManager;
    @Getter
    private Locations locations;
    @Getter
    private Inventorys inventorys;
    @Getter
    private CloudServer cloudServer;

    @Getter
    private final String prefix = "§8[§b§lR§3§lavenix§8] §7";

    @Override
    public void onEnable() {

        instance = this;

        this.cloudServer = new CloudServer();
        this.locations = new Locations();
        this.inventorys = new Inventorys();
        this.scoreboardManager = new ScoreboardManager();

        PluginManager pluginManager = Bukkit.getPluginManager();
        pluginManager.registerEvents(new InventoryClickListener(), this);
        pluginManager.registerEvents(new JoinQuitListener(), this);
        pluginManager.registerEvents(new PlayerInteractListener(), this);
        pluginManager.registerEvents(new ProtectionManager(), this);
        pluginManager.registerEvents(new InteractAtEntityListener(), this);

        worldControl();
        CloudNetDriver.getInstance().getEventManager().registerListener(new PubSubListener());
    }

    private void worldControl() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, () -> {
            for (World world : Bukkit.getWorlds()) {
                world.setDifficulty(Difficulty.PEACEFUL);
                world.setAutoSave(true);
                world.setStorm(false);
                world.setThundering(false);
                world.setAmbientSpawnLimit(0);
                world.setMonsterSpawnLimit(0);
                world.setTime(1000);
            }
        }, 0L, 20L);
    }

    @Override
    public void onDisable() {

    }

}
