package net.ravenix.lobbysystem.listener;

import net.ravenix.lobbysystem.utils.Locations;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.util.Vector;

public class ProtectionManager
        implements Listener {

    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        Player player = event.getPlayer();
        if (player.getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        event.setCancelled(true);
    }

    @EventHandler
    public void onPlace(BlockPlaceEvent event) {
        Player player = event.getPlayer();
        if (player.getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        event.setCancelled(true);
    }

    @EventHandler
    public void onHunger(FoodLevelChangeEvent event) { event.setCancelled(true); }

    @EventHandler
    public void onDamage(EntityDamageEvent event) { event.setCancelled(true); }

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();

        if (player.getLocation().getBlockY() <= 60) {
            player.teleport(Locations.spawn);
        }
        if(player.getGameMode() == GameMode.SURVIVAL) {
            if(player.getLocation().add(0,-1, 0).getBlock().getType() != Material.AIR) {
                player.setAllowFlight(true);
                player.setFlying(false);
            }
        }
    }

    @EventHandler
    public void handle(PlayerToggleFlightEvent event) {
        Player player = event.getPlayer();
        if(player.getGameMode() == GameMode.SURVIVAL) {
            event.setCancelled(true);
            player.setAllowFlight(false);
            player.setFlying(false);
            player.setVelocity(player.getLocation().getDirection().multiply(1).add(new Vector(0,1.0,0)));
        }
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        if (player.getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        event.setCancelled(true);
    }

    @EventHandler
    public void onWeather(WeatherChangeEvent event) { event.setCancelled(true); }

    @EventHandler
    public void onAchievement(PlayerAchievementAwardedEvent event){ event.setCancelled(true); }

    @EventHandler
    public void onDrop(PlayerDropItemEvent event) { event.setCancelled(true); }

    @EventHandler
    public void entitySpawn(CreatureSpawnEvent event) {
        boolean isCustom = event.getSpawnReason().equals(CreatureSpawnEvent.SpawnReason.CUSTOM);
        event.setCancelled(!isCustom);
    }

    @EventHandler
    public void onPickUpItem(PlayerPickupItemEvent event) { event.setCancelled(true); }

    @EventHandler
    public void onDamageOther(EntityDamageByEntityEvent event) { event.setCancelled(true); }

    @EventHandler
    public void onDamageBlocks(EntityDamageByBlockEvent event) { event.setCancelled(true); }

    @EventHandler
    public void handle(EntityExplodeEvent event) { event.setCancelled(true); }
}
