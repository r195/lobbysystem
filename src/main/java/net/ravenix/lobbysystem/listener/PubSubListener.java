package net.ravenix.lobbysystem.listener;

import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.event.EventListener;
import de.dytanic.cloudnet.driver.event.events.channel.ChannelMessageReceiveEvent;
import net.ravenix.clansystem.bukkit.BukkitClan;
import net.ravenix.clansystem.shared.clan.Clan;
import net.ravenix.clansystem.shared.clan.IClanProvider;
import net.ravenix.core.bukkit.BukkitCore;
import net.ravenix.core.shared.player.ICorePlayer;
import net.ravenix.core.shared.player.ICorePlayerProvider;
import net.ravenix.friendsystem.bukkit.BukkitFriends;
import net.ravenix.friendsystem.shared.friend.IFriendProvider;
import net.ravenix.lobbysystem.LobbySystem;
import net.ravenix.lobbysystem.scoreboard.ScoreboardManager;
import net.ravenix.lobbysystem.utils.Inventorys;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

public final class PubSubListener {

    @EventListener
    public void channelMessage(ChannelMessageReceiveEvent event) {
        JsonDocument jsonDocument = event.getData();

        ScoreboardManager scoreboardManager = LobbySystem.getInstance().getScoreboardManager();
        IFriendProvider friendProvider = BukkitFriends.getInstance().getFriendProvider();
        ICorePlayerProvider corePlayerProvider = BukkitCore.getInstance().getCorePlayerProvider();

        if (event.getChannel().equalsIgnoreCase("friendJoin")) {
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));

            ICorePlayer corePlayer = corePlayerProvider.getCorePlayer(uuid);
            if (corePlayer == null)
                corePlayerProvider.corePlayerConnect(uuid, "Server", "Proxy", false);

            List<UUID> friends = friendProvider.getFriends(uuid);
            if (friends == null) return;
            friends.forEach(friendUUID -> {
                Player player = Bukkit.getPlayer(friendUUID);
                if (player != null) {
                    scoreboardManager.applyScoreboard(player);
                }
            });
        }

        if (event.getChannel().equalsIgnoreCase("friendQuit")) {
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));

            ICorePlayer corePlayer = corePlayerProvider.getCorePlayer(uuid);
            if (corePlayer != null)
                corePlayerProvider.corePlayerDisconnect(uuid, false);

            friendProvider.getFriends(uuid).forEach(friendUUID -> {
                Player player = Bukkit.getPlayer(friendUUID);
                if (player != null) {
                    scoreboardManager.applyScoreboard(player);
                }
            });
        }

        if (event.getChannel().equalsIgnoreCase("createPermissionUser")) {
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));

            Player player = Bukkit.getPlayer(uuid);
            Bukkit.getScheduler().runTaskLater(LobbySystem.getInstance(), () -> {
                if (player != null) {
                    Inventorys inventorys = LobbySystem.getInstance().getInventorys();
                    inventorys.setHotbar(player);
                }
            }, 10L);
        }

        if (event.getChannel().equalsIgnoreCase("permissionUpdate")) {
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));

            Player player = Bukkit.getPlayer(uuid);
            if (player != null) {
                LobbySystem.getInstance().getScoreboardManager().applyScoreboard(player);
                Inventorys inventorys = LobbySystem.getInstance().getInventorys();
                inventorys.setHotbar(player);
            }
        }

        if (event.getChannel().equalsIgnoreCase("createCoinUser")) {
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            Player player = Bukkit.getPlayer(uuid);
            if (player != null)
                LobbySystem.getInstance().getScoreboardManager().applyScoreboard(player);
        }

        if (event.getChannel().equalsIgnoreCase("addCoins")) {
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            Player player = Bukkit.getPlayer(uuid);
            if (player != null)
                LobbySystem.getInstance().getScoreboardManager().applyScoreboard(player);
        }

        if (event.getChannel().equalsIgnoreCase("removeCoins")) {
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            Player player = Bukkit.getPlayer(uuid);
            if (player != null)
                LobbySystem.getInstance().getScoreboardManager().applyScoreboard(player);
        }

        if (event.getChannel().equalsIgnoreCase("clanKick")) {
            UUID uuid = UUID.fromString(jsonDocument.getString("toKick"));
            Player player = Bukkit.getPlayer(uuid);
            if (player != null)
                LobbySystem.getInstance().getScoreboardManager().applyScoreboard(player);
        }

        if (event.getChannel().equalsIgnoreCase("quitClan")) {
            UUID uuid = UUID.fromString(jsonDocument.getString("toQuit"));
            Player player = Bukkit.getPlayer(uuid);
            if (player != null)
                LobbySystem.getInstance().getScoreboardManager().applyScoreboard(player);
        }

        if (event.getChannel().equalsIgnoreCase("joinClan")) {
            UUID uuid = UUID.fromString(jsonDocument.getString("toJoin"));
            Player player = Bukkit.getPlayer(uuid);
            if (player != null)
                LobbySystem.getInstance().getScoreboardManager().applyScoreboard(player);
        }

        if (event.getChannel().equalsIgnoreCase("clanDelete")) {
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            Player player = Bukkit.getPlayer(uuid);
            if (player != null)
                LobbySystem.getInstance().getScoreboardManager().applyScoreboard(player);
        }

        if (event.getChannel().equalsIgnoreCase("createClan")) {
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            Player player = Bukkit.getPlayer(uuid);
            if (player != null)
                LobbySystem.getInstance().getScoreboardManager().applyScoreboard(player);
        }

        if (event.getChannel().equalsIgnoreCase("updateClanColor")) {
            String clanName = jsonDocument.getString("clanName");
            IClanProvider clanProvider = BukkitClan.getInstance().getClanProvider();
            Clan clan = clanProvider.getClanByName(clanName);
            if (clan == null) return;
            clan.getMembers().forEach(clanMember -> {
                Player player = Bukkit.getPlayer(clanMember.getUuid());
                if (player != null)
                    LobbySystem.getInstance().getScoreboardManager().applyScoreboard(player);
            });
        }
    }
}