package net.ravenix.lobbysystem.listener;

import net.ravenix.lobbysystem.LobbySystem;
import net.ravenix.lobbysystem.scoreboard.ScoreboardManager;
import net.ravenix.lobbysystem.utils.Inventorys;
import net.ravenix.lobbysystem.utils.Locations;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class JoinQuitListener
        implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        event.setJoinMessage(null);

        player.setGameMode(GameMode.SURVIVAL);

        ScoreboardManager scoreboardManager = LobbySystem.getInstance().getScoreboardManager();
        scoreboardManager.applyScoreboard(player);

        player.teleport(Locations.spawn);

        Inventorys inventorys = LobbySystem.getInstance().getInventorys();
        inventorys.setHotbar(player);
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        event.setQuitMessage(null);
    }

}
