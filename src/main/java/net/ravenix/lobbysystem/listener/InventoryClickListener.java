package net.ravenix.lobbysystem.listener;

import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.ext.bridge.player.IPlayerManager;
import net.ravenix.core.bukkit.BukkitCore;
import net.ravenix.core.shared.name.INameStorageProvider;
import net.ravenix.core.shared.name.NameResult;
import net.ravenix.core.shared.permission.IPermissionProvider;
import net.ravenix.core.shared.permission.group.IPermissionGroup;
import net.ravenix.core.shared.player.ICorePlayer;
import net.ravenix.core.shared.player.ICorePlayerProvider;
import net.ravenix.lobbysystem.LobbySystem;
import net.ravenix.lobbysystem.utils.Inventorys;
import net.ravenix.lobbysystem.utils.MultiPage;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import java.util.UUID;

public class InventoryClickListener implements Listener {

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        if (event.getClickedInventory() == null) return;
        if (event.getCurrentItem() == null) return;
        if (event.getCurrentItem().getItemMeta() == null) return;
        if (event.getCurrentItem().getType().equals(Material.STAINED_GLASS_PANE)) {
            event.setCancelled(true);
            return;
        }

        if (event.getInventory().getTitle().equals("§aFreunde")) {
            if (event.getCurrentItem().getType().equals(Material.SKULL_ITEM)) {
                if (event.getCurrentItem().getItemMeta().getDisplayName().equals("§aFreunde")) return;

                String playerName = getPlayerName(event.getCurrentItem().getItemMeta().getDisplayName());
                ICorePlayerProvider corePlayerProvider = BukkitCore.getInstance().getCorePlayerProvider();
                INameStorageProvider nameStorageProvider = BukkitCore.getInstance().getNameStorageProvider();
                NameResult nameResultByName = nameStorageProvider.getNameResultByName(playerName);
                LobbySystem.getInstance().getInventorys().openFriendMenu(playerName, (Player) event.getWhoClicked(), event.getCurrentItem(), corePlayerProvider.getCorePlayer(nameResultByName.getUuid()) == null);
            }
            /**
             *
             */
            if (event.getCurrentItem().getType().equals(Material.PAPER)) {
                Inventorys inventorys = LobbySystem.getInstance().getInventorys();
                Player player = (Player) event.getWhoClicked();
                new MultiPage(inventorys.getFriendsRequests(player), "§aFreundschaftsanfragen", player);
                return;
            }
            if (event.getCurrentItem().getType().equals(Material.CAKE)) {
                Inventorys inventorys = LobbySystem.getInstance().getInventorys();
                Player player = (Player) event.getWhoClicked();
                new MultiPage(inventorys.getPublicParties(), "§5Öffentliche Partys", player);
            }
            if (event.getCurrentItem().getType().equals(Material.FIREWORK)) {
                Inventorys inventorys = LobbySystem.getInstance().getInventorys();
                Player player = (Player) event.getWhoClicked();
                new MultiPage(inventorys.getPartymembers(player), "§5Party", player);
                return;
            }
            if (event.getCurrentItem().getType().equals(Material.IRON_CHESTPLATE)) {
                Inventorys inventorys = LobbySystem.getInstance().getInventorys();
                Player player = (Player) event.getWhoClicked();
                new MultiPage(inventorys.getClanmembers(player), "§6Clan", player);
                return;
            }
        }

        if (event.getInventory().getTitle().equals("§5Party")) {
            if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§aParty erstellen")) {
                executeCommand(event.getWhoClicked().getUniqueId(), "party create");
                Inventorys inventorys = LobbySystem.getInstance().getInventorys();
                Player player = (Player) event.getWhoClicked();
                Bukkit.getScheduler().runTaskLater(LobbySystem.getInstance(), () -> {
                    new MultiPage(inventorys.getPartymembers(player), "§5Party", player);
                }, 2L);
                Bukkit.getScheduler().runTaskLater(LobbySystem.getInstance(), () -> {
                    new MultiPage(inventorys.getPartymembers(player), "§5Party", player);
                }, 5L);
            }
            if (event.getCurrentItem().getType().equals(Material.CAKE)) {
                Inventorys inventorys = LobbySystem.getInstance().getInventorys();
                Player player = (Player) event.getWhoClicked();
                new MultiPage(inventorys.getPublicParties(), "§5Öffentliche Partys", player);
            }
            if (event.getCurrentItem().getType().equals(Material.PAPER)) {
                Inventorys inventorys = LobbySystem.getInstance().getInventorys();
                Player player = (Player) event.getWhoClicked();
                new MultiPage(inventorys.getFriendsRequests(player), "§aFreundschaftsanfragen", player);
                return;
            }
            if (event.getCurrentItem().getItemMeta().getDisplayName().equals("§aFreunde")) {
                Inventorys inventorys = LobbySystem.getInstance().getInventorys();
                Player player = (Player) event.getWhoClicked();
                new MultiPage(inventorys.getFriends(player), "§aFreunde", player);
                return;
            }
            if (event.getCurrentItem().getType().equals(Material.IRON_CHESTPLATE)) {
                Inventorys inventorys = LobbySystem.getInstance().getInventorys();
                Player player = (Player) event.getWhoClicked();
                new MultiPage(inventorys.getClanmembers(player), "§6Clan", player);
                return;
            }

            if (event.getCurrentItem().getType().equals(Material.SKULL_ITEM)) {
                String playerName = getPlayerName(event.getCurrentItem().getItemMeta().getDisplayName());
                if (playerName.equals(event.getWhoClicked().getName())) {
                    return;
                }
                INameStorageProvider nameStorageProvider = BukkitCore.getInstance().getNameStorageProvider();
                LobbySystem.getInstance().getInventorys().openPartyMenu(playerName, (Player) event.getWhoClicked(), event.getCurrentItem());
                return;
            }
        }

        if (event.getInventory().getTitle().equals("§6Clan")) {
            if (event.getCurrentItem().getItemMeta().getDisplayName().equals("§aFreunde")) {
                Inventorys inventorys = LobbySystem.getInstance().getInventorys();
                Player player = (Player) event.getWhoClicked();
                new MultiPage(inventorys.getFriends(player), "§aFreunde", player);
                return;
            }
            if (event.getCurrentItem().getType().equals(Material.CAKE)) {
                Inventorys inventorys = LobbySystem.getInstance().getInventorys();
                Player player = (Player) event.getWhoClicked();
                new MultiPage(inventorys.getPublicParties(), "§5Öffentliche Partys", player);
            }
            if (event.getCurrentItem().getType().equals(Material.FIREWORK)) {
                Inventorys inventorys = LobbySystem.getInstance().getInventorys();
                Player player = (Player) event.getWhoClicked();
                new MultiPage(inventorys.getPartymembers(player), "§5Party", player);
                return;
            }

            if (event.getCurrentItem().getType().equals(Material.IRON_CHESTPLATE)) {
                Inventorys inventorys = LobbySystem.getInstance().getInventorys();
                Player player = (Player) event.getWhoClicked();
                new MultiPage(inventorys.getClanmembers(player), "§6Clan", player);
                return;
            }

            if (event.getCurrentItem().getType().equals(Material.PAPER)) {
                Inventorys inventorys = LobbySystem.getInstance().getInventorys();
                Player player = (Player) event.getWhoClicked();
                new MultiPage(inventorys.getFriendsRequests(player), "§aFreundschaftsanfragen", player);
                return;
            }

            if (event.getCurrentItem().getType().equals(Material.SKULL_ITEM)) {
                String playerName = getPlayerName(event.getCurrentItem().getItemMeta().getDisplayName());
                if (playerName.equals(event.getWhoClicked().getName())) {
                    return;
                }
                LobbySystem.getInstance().getInventorys().openClanMenu(playerName, (Player) event.getWhoClicked(), event.getCurrentItem());
                return;
            }
        }

        if (event.getInventory().getTitle().equals("§5Öffentliche Partys")) {
            if (event.getCurrentItem().getItemMeta().getDisplayName().equals("§aFreunde")) {
                Inventorys inventorys = LobbySystem.getInstance().getInventorys();
                Player player = (Player) event.getWhoClicked();
                new MultiPage(inventorys.getFriends(player), "§aFreunde", player);
                return;
            }
            if (event.getCurrentItem().getType().equals(Material.PAPER)) {
                Inventorys inventorys = LobbySystem.getInstance().getInventorys();
                Player player = (Player) event.getWhoClicked();
                new MultiPage(inventorys.getFriendsRequests(player), "§aFreundschaftsanfragen", player);
                return;
            }
            if (event.getCurrentItem().getType().equals(Material.IRON_CHESTPLATE)) {
                Inventorys inventorys = LobbySystem.getInstance().getInventorys();
                Player player = (Player) event.getWhoClicked();
                new MultiPage(inventorys.getClanmembers(player), "§6Clan", player);
                return;
            }
            if (event.getCurrentItem().getType().equals(Material.FIREWORK)) {
                Inventorys inventorys = LobbySystem.getInstance().getInventorys();
                Player player = (Player) event.getWhoClicked();
                new MultiPage(inventorys.getPartymembers(player), "§5Party", player);
                return;
            }

            if (event.getCurrentItem().getType().equals(Material.SKULL_ITEM)) {
                String displayName = event.getCurrentItem().getItemMeta().getDisplayName().replace("§5Party von ", "");
                String playerName = getPlayerName(displayName);
                if (playerName.equals(event.getWhoClicked().getName())) {
                    return;
                }
                executeCommand(event.getWhoClicked().getUniqueId(), "party join " + playerName);
                Player player = (Player) event.getWhoClicked();
                Inventorys inventorys = LobbySystem.getInstance().getInventorys();
                Bukkit.getScheduler().runTaskLater(LobbySystem.getInstance(), () -> {
                    new MultiPage(inventorys.getPublicParties(), "§5Öffentliche Partys", player);
                } ,4L);
                return;
            }
        }

        if (event.getInventory().getTitle().equals("§aFreundschaftsanfragen")) {
            if (event.getCurrentItem().getItemMeta().getDisplayName().equals("§aFreunde")) {
                Inventorys inventorys = LobbySystem.getInstance().getInventorys();
                Player player = (Player) event.getWhoClicked();
                new MultiPage(inventorys.getFriends(player), "§aFreunde", player);
                return;
            }
            if (event.getCurrentItem().getType().equals(Material.CAKE)) {
                Inventorys inventorys = LobbySystem.getInstance().getInventorys();
                Player player = (Player) event.getWhoClicked();
                new MultiPage(inventorys.getPublicParties(), "§5Öffentliche Partys", player);
            }
            if (event.getCurrentItem().getType().equals(Material.IRON_CHESTPLATE)) {
                Inventorys inventorys = LobbySystem.getInstance().getInventorys();
                Player player = (Player) event.getWhoClicked();
                new MultiPage(inventorys.getClanmembers(player), "§6Clan", player);
                return;
            }
            if (event.getCurrentItem().getType().equals(Material.FIREWORK)) {
                Inventorys inventorys = LobbySystem.getInstance().getInventorys();
                Player player = (Player) event.getWhoClicked();
                new MultiPage(inventorys.getPartymembers(player), "§5Party", player);
                return;
            }

            if (event.getCurrentItem().getType().equals(Material.SKULL_ITEM)) {
                String playerName = getPlayerName(event.getCurrentItem().getItemMeta().getDisplayName());
                if (playerName.equals(event.getWhoClicked().getName())) {
                    return;
                }
                if (event.getClick().isRightClick()) {
                    executeCommand(event.getWhoClicked().getUniqueId(), "friend deny " + playerName);
                }
                if (event.getClick().isLeftClick()) {
                    executeCommand(event.getWhoClicked().getUniqueId(), "friend accept " + playerName);
                }
                Player player = (Player) event.getWhoClicked();
                Inventorys inventorys = LobbySystem.getInstance().getInventorys();
                Bukkit.getScheduler().runTaskLater(LobbySystem.getInstance(), () -> {
                    new MultiPage(inventorys.getFriendsRequests(player), "§aFreundschaftsanfragen", player);
                } ,4L);
                return;
            }
        }


        if (event.getInventory().getTitle().startsWith("§aFreunde §8┃ ")) {
            String playerName = getPlayerName(event.getInventory().getTitle().replace("§aFreunde §8┃ ", ""));
            if (event.getCurrentItem().getType().equals(Material.IRON_CHESTPLATE)) {
                executeCommand(event.getWhoClicked().getUniqueId(), "clan invite " + playerName);
                event.getWhoClicked().closeInventory();
            }
            if (event.getCurrentItem().getType().equals(Material.CAKE)) {
                executeCommand(event.getWhoClicked().getUniqueId(), "party invite " + playerName);
                event.getWhoClicked().closeInventory();
            }
            if (event.getCurrentItem().getType().equals(Material.BARRIER)) {
                executeCommand(event.getWhoClicked().getUniqueId(), "friend remove " + playerName);
                event.getWhoClicked().closeInventory();
            }
            if (event.getCurrentItem().getType().equals(Material.ENDER_PEARL)) {
                executeCommand(event.getWhoClicked().getUniqueId(), "friend jump " + playerName);
                event.getWhoClicked().closeInventory();
            }
        }
        if (event.getInventory().getTitle().startsWith("§5Party ")) {
            String playerName = getPlayerName(event.getInventory().getTitle().replace("§5Party §8┃ ", ""));
            if (event.getCurrentItem().getType().equals(Material.PAPER)) {
                executeCommand(event.getWhoClicked().getUniqueId(), "friend add " + playerName);
                event.getWhoClicked().closeInventory();
            }
            if (event.getCurrentItem().getType().equals(Material.BARRIER)) {
                executeCommand(event.getWhoClicked().getUniqueId(), "party kick " + playerName);
                event.getWhoClicked().closeInventory();
            }
        }
        if (event.getInventory().getTitle().startsWith("§6Clan ")) {
            String playerName = getPlayerName(event.getInventory().getTitle().replace("§6Clan §8┃ ", ""));
            if (event.getCurrentItem().getType().equals(Material.PAPER)) {
                executeCommand(event.getWhoClicked().getUniqueId(), "friend add " + playerName);
                event.getWhoClicked().closeInventory();
            }
            if (event.getCurrentItem().getType().equals(Material.BARRIER)) {
                executeCommand(event.getWhoClicked().getUniqueId(), "clan kick " + playerName);
                event.getWhoClicked().closeInventory();
            }
            if (event.getCurrentItem().getType().equals(Material.CAKE)) {
                executeCommand(event.getWhoClicked().getUniqueId(), "party invite " + playerName);
                event.getWhoClicked().closeInventory();
            }
        }

        if (event.getClickedInventory().getTitle().startsWith("§bDu bist auf")) {
            ICorePlayerProvider corePlayerProvider = BukkitCore.getInstance().getCorePlayerProvider();
            ICorePlayer corePlayer = corePlayerProvider.getCorePlayer(event.getWhoClicked().getUniqueId());
            if (ChatColor.stripColor(event.getCurrentItem().getItemMeta().getDisplayName()).equals(corePlayer.getServer())) {
                event.getWhoClicked().sendMessage(LobbySystem.getInstance().getPrefix() + "§cDu bist bereits auf dieser Lobby.");
                event.setCancelled(true);
                return;
            }
            String server = ChatColor.stripColor(event.getCurrentItem().getItemMeta().getDisplayName());
            if (event.getCurrentItem().getType().equals(Material.STORAGE_MINECART)) {

                CloudNetDriver.getInstance().getServicesRegistry().getFirstService(IPlayerManager.class)
                        .getPlayerExecutor(event.getWhoClicked().getUniqueId())
                        .connect(server);

                event.setCancelled(true);
            }
        }
    }

    private String getPlayerName(String displayname) {
        IPermissionProvider permissionProvider = BukkitCore.getInstance().getPermissionProvider();
        for (IPermissionGroup permissionGroup : permissionProvider.getPermissionGroups()) {
            displayname = displayname.replace(permissionGroup.getColor(), "");
        }
        return displayname;
    }

    private void executeCommand(UUID uuid, String command) {
        JsonDocument jsonDocument = new JsonDocument();
        jsonDocument.append("uuid", uuid);
        jsonDocument.append("command", command);

        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("executeCommand", "update", jsonDocument);
    }

    /**
     * Freundekopf Multipage
     */

    @EventHandler(ignoreCancelled = true)
    public void onClick(InventoryClickEvent event) {
        if (!(event.getWhoClicked() instanceof Player)) return;
        Player player = (Player) event.getWhoClicked();

        if (!MultiPage.users.containsKey(player.getUniqueId())) return;
        MultiPage inv = MultiPage.users.get(player.getUniqueId());
        if (event.getCurrentItem() == null) return;
        if (event.getCurrentItem().getItemMeta() == null) return;
        if (event.getCurrentItem().getItemMeta().getDisplayName() == null) return;

        if (event.getCurrentItem().getItemMeta().getDisplayName().equals("§bNächste Seite")) {
            event.setCancelled(true);
            if (inv.currpage >= inv.pages.size() - 1) {
                player.sendMessage(LobbySystem.getInstance().getPrefix() + "§cEs gibt keine weitere Seite.");
                return;
            }
            inv.currpage += 1;
            player.openInventory(inv.pages.get(inv.currpage));
        } else if (event.getCurrentItem().getItemMeta().getDisplayName().equals("§bLetzte Seite")) {
            event.setCancelled(true);
            if (inv.currpage > 0) {
                inv.currpage -= 1;
                player.openInventory(inv.pages.get(inv.currpage));
                return;
            }
            player.sendMessage(LobbySystem.getInstance().getPrefix() + "§cDu bist bereits auf der ersten Seite.");
        }


    }
}
