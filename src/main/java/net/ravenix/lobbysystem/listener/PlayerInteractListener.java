package net.ravenix.lobbysystem.listener;

import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import net.ravenix.lobbysystem.LobbySystem;
import net.ravenix.lobbysystem.utils.Inventorys;
import net.ravenix.lobbysystem.utils.MultiPage;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.UUID;

public class PlayerInteractListener implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        Material item = player.getItemInHand().getType();

        Inventorys inventorys = LobbySystem.getInstance().getInventorys();

        if (event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            if (item.equals(Material.COMPASS)) {
                inventorys.openCompassInventory(player);
                return;
            }

            if (item.equals(Material.NETHER_STAR)) {
                inventorys.openLobbySwitcherInventory(player);
            }

            if (item.equals(Material.NAME_TAG)) {
                Bukkit.dispatchCommand(player, "nick");
            }

            if (item.equals(Material.SKULL_ITEM)) {
                new MultiPage(inventorys.getFriends(player), "§aFreunde", player);
            }
        }
    }

    @EventHandler
    public void entityDamageByEntityEvent(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player && event.getEntity() instanceof Player) {
            Player toAdd = (Player) event.getEntity();
            Player player = (Player) event.getDamager();

            if (player.getItemInHand().getType().equals(Material.SKULL_ITEM)) {
                executeCommand(player.getUniqueId(), "friend add " + toAdd.getName());
            }
        }
    }

    private void executeCommand(UUID uuid, String command) {
        JsonDocument jsonDocument = new JsonDocument();
        jsonDocument.append("uuid", uuid);
        jsonDocument.append("command", command);

        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("executeCommand", "update", jsonDocument);
    }

}
