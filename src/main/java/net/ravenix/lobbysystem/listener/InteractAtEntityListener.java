package net.ravenix.lobbysystem.listener;

import com.google.common.collect.Lists;
import net.ravenix.lobbysystem.LobbySystem;
import net.ravenix.lobbysystem.utils.Inventorys;
import net.ravenix.lobbysystem.utils.MultiPage;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;

public final class InteractAtEntityListener
        implements Listener {

    @EventHandler
    public void interactAtEntity(PlayerInteractAtEntityEvent event) {
        Inventorys inventorys = LobbySystem.getInstance().getInventorys();
        Player player = event.getPlayer();
        if (event.getRightClicked().getCustomName().equalsIgnoreCase("§5Öffentliche Partys")) {
            new MultiPage(inventorys.getPublicParties(), "§5Öffentliche Partys", player);
        }
        if (event.getRightClicked().getCustomName().equalsIgnoreCase("§3Replays")) {
            new MultiPage(Lists.newArrayList(), "§3Replays", player);
        }
    }

}
