package net.ravenix.lobbysystem.utils;

import com.google.common.collect.Lists;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.driver.service.ServiceInfoSnapshot;
import de.dytanic.cloudnet.ext.bridge.BridgeServiceProperty;
import de.dytanic.cloudnet.ext.bridge.ServiceInfoSnapshotUtil;
import net.ravenix.core.bukkit.util.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class CloudServer {

    public List<ItemStack> getServers(String servers){
        List<ItemStack> list = Lists.newArrayList();
        Collection<ServiceInfoSnapshot> lobbys
                = CloudNetDriver.getInstance().getCloudServiceProvider().getCloudServicesByGroup(servers).stream().sorted().collect(Collectors.toList());
        for(ServiceInfoSnapshot server : lobbys){
            if(ServiceInfoSnapshotUtil.isOnline(server)){
                list.add(new ItemBuilder(Material.STORAGE_MINECART, server.getProperty(BridgeServiceProperty.ONLINE_COUNT).orElse(0))
                        .name(server.getServiceId().getName()).itemStack());
            }
        }

        return list;
    }

}
