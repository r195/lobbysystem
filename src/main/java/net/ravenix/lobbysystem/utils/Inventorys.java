package net.ravenix.lobbysystem.utils;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.ravenix.clansystem.bukkit.BukkitClan;
import net.ravenix.clansystem.shared.clan.Clan;
import net.ravenix.clansystem.shared.clan.IClanProvider;
import net.ravenix.core.bukkit.BukkitCore;
import net.ravenix.core.bukkit.util.ItemBuilder;
import net.ravenix.core.shared.name.INameStorageProvider;
import net.ravenix.core.shared.name.NameResult;
import net.ravenix.core.shared.permission.IPermissionProvider;
import net.ravenix.core.shared.permission.PrefixType;
import net.ravenix.core.shared.permission.user.IPermissionUser;
import net.ravenix.core.shared.player.ICorePlayer;
import net.ravenix.core.shared.player.ICorePlayerProvider;
import net.ravenix.core.shared.skin.ISkin;
import net.ravenix.core.shared.skin.ISkinProvider;
import net.ravenix.core.shared.util.TimeFormat;
import net.ravenix.friendsystem.bukkit.BukkitFriends;
import net.ravenix.friendsystem.shared.friend.IFriendProvider;
import net.ravenix.lobbysystem.LobbySystem;
import net.ravenix.partysystem.bukkit.BukkitParty;
import net.ravenix.partysystem.shared.party.IPartyProvider;
import net.ravenix.partysystem.shared.party.Party;
import net.ravenix.playtime.bukkit.BukkitPlaytime;
import net.ravenix.playtime.shared.playtime.IPlaytimeProvider;
import net.ravenix.playtime.shared.user.IPlaytimeUser;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class Inventorys {

    public void setHotbar(Player player) {
        player.getInventory().clear();
        player.getInventory()
                .setItem(0, new ItemAPI(Material.COMPASS).setName("§bServer Switcher").setLore("§7Verbinde dich zu einem §bSpielmodus§7.").toItemStack());
        player.getInventory()
                .setItem(1, new ItemAPI(Material.NETHER_STAR).setName("§bLobby Switcher").setLore("§7Verbinde dich zu einer anderen §bLobby§7.").toItemStack());
        if (player.hasPermission("ravenix.command.nick")) {
            player.getInventory()
                    .setItem(4, new ItemAPI(Material.NAME_TAG).setName("§bNick").setLore("§7Verstecke dich §7vor anderen Spielern, um deine §5Privatspähre §7zu genießen.").toItemStack());
        }
        player.getInventory()
                .setItem(7, new ItemAPI(Material.ENDER_CHEST).setName("§bCosmetics").setLore("§7Wähle §bCosmetics §7wie §eHüte§7, §eSchuhe§7, §eBallons§7 oder §eHaustiere§7 aus.").toItemStack());
        ISkinProvider skinProvider = BukkitCore.getInstance().getSkinProvider();
        ISkin skin = skinProvider.getSkin(player.getUniqueId());
        player.getInventory()
                .setItem(8,
                        new ItemBuilder(Material.SKULL_ITEM, 1, 3)
                                .headValue(skin.getSkinValue()).name("§bProfil").lore("§7Verwalte §aFreunde§7, §5Party §7und §6Clan§7.").itemStack());
    }

    public void openCompassInventory(Player player) {
        Inventory inventory = Bukkit.createInventory(null, 6 * 9, "§bServer Switcher");

        for (int i = 0; i < inventory.getSize(); i++) {
            inventory.setItem(i, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 7).name("§c").itemStack());
        }
        inventory.setItem(0, new ItemBuilder(Material.STORAGE_MINECART).itemStack());
        player.openInventory(inventory);
    }

    public void openLobbySwitcherInventory(Player player) {
        ICorePlayerProvider corePlayerProvider = BukkitCore.getInstance().getCorePlayerProvider();
        ICorePlayer corePlayer = corePlayerProvider.getCorePlayer(player.getUniqueId());

        int lobbys = LobbySystem.getInstance().getCloudServer().getServers("Lobby").size();
        int size = 9;

        while (lobbys > size) {
            size += 9;
        }

        Inventory inventory = Bukkit.createInventory(null, size, "§bDu bist auf §e" + corePlayer.getServer());

        for (ItemStack itemStack : LobbySystem.getInstance().getCloudServer().getServers("Lobby")) {
            if (itemStack.getItemMeta().getDisplayName().equals(corePlayer.getServer())) {
                inventory.addItem(new ItemAPI(Material.STORAGE_MINECART, itemStack.getAmount()).addEnchant(Enchantment.KNOCKBACK, 1).setName("§b" + itemStack.getItemMeta().getDisplayName()).setItemFlag(ItemFlag.HIDE_ENCHANTS).toItemStack());
            } else {
                inventory.addItem(new ItemAPI(Material.STORAGE_MINECART, itemStack.getAmount()).setName("§b" + itemStack.getItemMeta().getDisplayName()).toItemStack());
            }

        }

        player.openInventory(inventory);
    }

    public void openFriendMenu(String playerName, Player player, ItemStack head, boolean offline) {

        INameStorageProvider nameStorageProvider = BukkitCore.getInstance().getNameStorageProvider();
        ICorePlayerProvider corePlayerProvider = BukkitCore.getInstance().getCorePlayerProvider();

        NameResult nameResult = nameStorageProvider.getNameResultByName(playerName);
        ICorePlayer corePlayer = corePlayerProvider.getCorePlayer(nameResult.getUuid());

        Inventory inventory = Bukkit.createInventory(null, 3 * 9, "§aFreunde §8┃ " + head.getItemMeta().getDisplayName());

        for (int i = 0; i < inventory.getSize(); i++) {
            inventory.setItem(i, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 7).name("§c").itemStack());
        }

        if (offline) {
            inventory.setItem(10, head);
            inventory.setItem(13, new ItemBuilder(Material.IRON_CHESTPLATE).name("§6In deinen Clan einladen").itemStack());
            inventory.setItem(16, new ItemBuilder(Material.BARRIER).name("§cFreund entfernen").itemStack());

            player.openInventory(inventory);
            return;
        }

        inventory.setItem(10, head);
        inventory.setItem(12, new ItemBuilder(Material.ENDER_PEARL).name("§bZum Spieler springen §8(§e" + corePlayer.getServer() + "§8)").itemStack());
        inventory.setItem(13, new ItemBuilder(Material.CAKE).name("§5In deine Party einladen").itemStack());
        inventory.setItem(14, new ItemBuilder(Material.IRON_CHESTPLATE).name("§6In deinen Clan einladen").itemStack());

        inventory.setItem(16, new ItemBuilder(Material.BARRIER).name("§cFreund entfernen").itemStack());

        player.openInventory(inventory);
    }

    public void openPartyMenu(String playerName, Player player, ItemStack head) {

        INameStorageProvider nameStorageProvider = BukkitCore.getInstance().getNameStorageProvider();
        ICorePlayerProvider corePlayerProvider = BukkitCore.getInstance().getCorePlayerProvider();

        NameResult nameResult = nameStorageProvider.getNameResultByName(playerName);
        ICorePlayer corePlayer = corePlayerProvider.getCorePlayer(nameResult.getUuid());

        Inventory inventory = Bukkit.createInventory(null, 3 * 9, "§5Party §8┃ " + head.getItemMeta().getDisplayName());

        for (int i = 0; i < inventory.getSize(); i++) {
            inventory.setItem(i, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 7).name("§c").itemStack());
        }

        inventory.setItem(11, head);
        inventory.setItem(13, new ItemBuilder(Material.BARRIER).name("§cAus der Party kicken").itemStack());
        inventory.setItem(15, new ItemBuilder(Material.PAPER).name("§aFreund hinzufügen").itemStack());

        player.openInventory(inventory);
    }

    public void openClanMenu(String playerName, Player player, ItemStack head) {

        INameStorageProvider nameStorageProvider = BukkitCore.getInstance().getNameStorageProvider();
        ICorePlayerProvider corePlayerProvider = BukkitCore.getInstance().getCorePlayerProvider();

        NameResult nameResult = nameStorageProvider.getNameResultByName(playerName);
        ICorePlayer corePlayer = corePlayerProvider.getCorePlayer(nameResult.getUuid());

        Inventory inventory = Bukkit.createInventory(null, 3 * 9, "§6Clan §8┃ " + head.getItemMeta().getDisplayName());

        for (int i = 0; i < inventory.getSize(); i++) {
            inventory.setItem(i, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 7).name("§c").itemStack());
        }


        if (corePlayer != null) {
            inventory.setItem(10, head);
            inventory.setItem(12, new ItemBuilder(Material.BARRIER).name("§cAus dem Clan kicken").itemStack());
            inventory.setItem(16, new ItemBuilder(Material.CAKE).name("§5In deine Party einladen").itemStack());
            inventory.setItem(14, new ItemBuilder(Material.PAPER).name("§aFreund hinzufügen").itemStack());
            player.openInventory(inventory);
            return;
        }

        inventory.setItem(11, head);
        inventory.setItem(13, new ItemBuilder(Material.BARRIER).name("§cAus dem Clan kicken").itemStack());
        inventory.setItem(15, new ItemBuilder(Material.PAPER).name("§aFreund hinzufügen").itemStack());

        player.openInventory(inventory);
    }

    public ArrayList<ItemStack> getFriends(Player player) {

        ArrayList<ItemStack> list = Lists.newArrayList();

        IFriendProvider ifriendProvider = BukkitFriends.getInstance().getFriendProvider();
        ICorePlayerProvider corePlayerProvider = BukkitCore.getInstance().getCorePlayerProvider();
        INameStorageProvider nameStorageProvider = BukkitCore.getInstance().getNameStorageProvider();
        List<Friend> friends = Lists.newArrayList();

        List<UUID> friendCache = ifriendProvider.getFriends(player.getUniqueId());
        if (friendCache != null) {
            friendCache.forEach(friendUUID -> {
                ICorePlayer corePlayer = corePlayerProvider.getCorePlayer(friendUUID);
                if (corePlayer != null) {
                    Friend friend = new Friend(friendUUID, true);
                    friends.add(friend);
                } else {
                    Friend friend = new Friend(friendUUID, false);
                    friends.add(friend);
                }
            });
        }

        ISkinProvider skinProvider = BukkitCore.getInstance().getSkinProvider();
        friends.stream().sorted(Comparator.comparing(Friend::isOnline).reversed()).forEach(friend -> {
            IPermissionUser permissionUser = BukkitCore.getInstance().getPermissionProvider().getPermissionUser(friend.getUuid());
            NameResult nameResult = nameStorageProvider.getNameResultByUUID(friend.getUuid());
            if (friend.isOnline()) {
                ICorePlayer corePlayer = BukkitCore.getInstance().getCorePlayerProvider().getCorePlayer(friend.getUuid());
                list.add(new ItemBuilder(Material.SKULL_ITEM, 1, 3).headValue(skinProvider
                                .getSkin(friend.getUuid())
                                .getSkinValue())
                        .name(permissionUser.getHighestGroup().getColor() + nameResult.getName()).lore("§7Status §8» §7Online auf: §e" + corePlayer.getServer())
                        .itemStack());
            } else {
                IPlaytimeProvider playtimeProvider = BukkitPlaytime.getInstance().getPlaytimeProvider();
                IPlaytimeUser playtimeUser = playtimeProvider.getPlaytimeUser(friend.getUuid());
                list.add(new ItemBuilder(Material.SKULL_ITEM, 1, 3)
                        .headValue("ewogICJ0aW1lc3RhbXAiIDogMTYyOTMwNTQzMjA4NCwKICAicHJvZmlsZUlkIi" +
                                "A6ICJhM2Y0MjdhODE4YzU0OWM1YTRmYjY0YzZlMGUxZTBhOCIsCiAgInByb2ZpbGVOYW1" +
                                "lIiA6ICJNSEZfU2tlbGV0b24iLAogICJzaWduYXR1cmVSZXF1aXJlZCIgOiB0cnVlLAogICJ" +
                                "0ZXh0dXJlcyIgOiB7CiAgICAiU0tJTiIgOiB7CiAgICAgICJ1cmwiIDogImh0dHA6Ly90ZXh0dX" +
                                "Jlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMjI3OTVjM2M2ZjM2ZDY3ZGVjZjlhMzE5NWUxMjgwNDB" +
                                "iZWM1MjI2YjA1NWYyYjE2ZDQ2ZmExOWE5MTgwZTAyMyIKICAgIH0KICB9Cn0=").name(permissionUser.getHighestGroup().getColor() + nameResult.getName()).lore("§7Status §8» §cOffline").lore("§7Letzter Login §8» §b" + TimeFormat.format(playtimeUser.getLastLogin())).itemStack());

            }
        });

        return list;
    }

    public ArrayList<ItemStack> getFriendsRequests(Player player) {

        ArrayList<ItemStack> list = Lists.newArrayList();

        IFriendProvider ifriendProvider = BukkitFriends.getInstance().getFriendProvider();
        INameStorageProvider nameStorageProvider = BukkitCore.getInstance().getNameStorageProvider();

        List<UUID> requestCache = ifriendProvider.getRequests(player.getUniqueId());
        if (requestCache == null) return Lists.newArrayList();

        ISkinProvider skinProvider = BukkitCore.getInstance().getSkinProvider();
        requestCache.forEach(uuid -> {
            IPermissionUser permissionUser = BukkitCore.getInstance().getPermissionProvider().getPermissionUser(uuid);
            NameResult nameResult = nameStorageProvider.getNameResultByUUID(uuid);
            list.add(new ItemBuilder(Material.SKULL_ITEM, 1, 3).headValue(skinProvider
                            .getSkin(uuid)
                            .getSkinValue())
                    .name(permissionUser.getHighestGroup().getColor() + nameResult.getName()).lore("§8» §aLinksklick zum annehmen").lore("§8» §cRechtsklick zum ablehnen")
                    .itemStack());
        });

        return list;
    }

    public ArrayList<ItemStack> getPublicParties() {
        List<Party> publicParties = BukkitParty.getInstance().getPartyProvider().getPublicParties();

        if (publicParties == null) return Lists.newArrayList();
        if (publicParties.size() == 0) return Lists.newArrayList();

        IPermissionProvider permissionProvider = BukkitCore.getInstance().getPermissionProvider();
        ICorePlayerProvider corePlayerProvider = BukkitCore.getInstance().getCorePlayerProvider();
        INameStorageProvider nameStorageProvider = BukkitCore.getInstance().getNameStorageProvider();

        ArrayList<ItemStack> itemStackList = Lists.newArrayList();

        publicParties.forEach(party -> {
            IPermissionUser permissionUser = permissionProvider.getPermissionUser(party.getLeader());
            NameResult nameResultByUUID = nameStorageProvider.getNameResultByUUID(party.getLeader());
            ICorePlayer corePlayer = corePlayerProvider.getCorePlayer(party.getLeader());
            itemStackList.add(new ItemBuilder(Material.SKULL_ITEM, 1, 3)
                    .name("§5Party von "
                            + permissionUser.getHighestGroup().getColor()
                            + nameResultByUUID.getName())
                    .lore("§7Online auf§8: §b" + corePlayer.getServer())
                    .lore("§7Mitglieder§8: §b" + party.getMembers().size())
                    .headValue("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZTJmNTYzOGMyMTNlYTJhNWE5NGU2NTk4MTY2YmM0N2VlZWI0MGY1ZGY4NGRhNzBjZDkyMzNjMTgxMmRmMTEwIn19fQ==").itemStack());
        });

        return itemStackList;
    }

    public ArrayList<ItemStack> getPartymembers(Player player) {

        ArrayList<ItemStack> list = new ArrayList<ItemStack>();

        IPartyProvider partyProvider = BukkitParty.getInstance().getPartyProvider();
        INameStorageProvider nameStorageProvider = BukkitCore.getInstance().getNameStorageProvider();

        Party party = partyProvider.getParty(player.getUniqueId());
        if (party == null) return Lists.newArrayList();


        List<UUID> members = party.getMembers();

        ISkinProvider skinProvider = BukkitCore.getInstance().getSkinProvider();
        members.forEach(partyMemberUUID -> {
            IPermissionUser permissionUser = BukkitCore.getInstance().getPermissionProvider().getPermissionUser(partyMemberUUID);
            NameResult nameResult = nameStorageProvider.getNameResultByUUID(partyMemberUUID);
            ICorePlayer corePlayer = BukkitCore.getInstance().getCorePlayerProvider().getCorePlayer(partyMemberUUID);
            list.add(new ItemBuilder(Material.SKULL_ITEM, 1, 3).headValue(skinProvider
                            .getSkin(partyMemberUUID)
                            .getSkinValue())
                    .name(permissionUser.getHighestGroup().getColor() + nameResult.getName()).lore("§7Status §8» §7Online auf: §e" + corePlayer.getServer())
                    .itemStack());
        });
        return list;
    }

    public ArrayList<ItemStack> getClanmembers(Player player) {

        ArrayList<ItemStack> list = new ArrayList<ItemStack>();

        IClanProvider clanProvider = BukkitClan.getInstance().getClanProvider();
        ICorePlayerProvider corePlayerProvider = BukkitCore.getInstance().getCorePlayerProvider();
        INameStorageProvider nameStorageProvider = BukkitCore.getInstance().getNameStorageProvider();

        Clan clan = clanProvider.getClan(player.getUniqueId());
        if (clan == null) return Lists.newArrayList();

        ISkinProvider skinProvider = BukkitCore.getInstance().getSkinProvider();
        clan.getMembers().forEach(clanMember -> {
            IPermissionUser permissionUser = BukkitCore.getInstance().getPermissionProvider().getPermissionUser(clanMember.getUuid());
            NameResult nameResult = nameStorageProvider.getNameResultByUUID(clanMember.getUuid());

            ICorePlayer corePlayer = BukkitCore.getInstance().getCorePlayerProvider().getCorePlayer(clanMember.getUuid());
            IPlaytimeProvider playtimeProvider = BukkitPlaytime.getInstance().getPlaytimeProvider();
            IPlaytimeUser playtimeUser = playtimeProvider.getPlaytimeUser(clanMember.getUuid());

            if (corePlayer == null) {
                list.add(
                        new ItemBuilder(Material.SKULL_ITEM, 1, 3).headValue("ewogICJ0aW1lc3RhbXAiIDogMTYyOTMwNTQzMjA4NCwKICAicHJvZmlsZUlkIi" +
                                        "A6ICJhM2Y0MjdhODE4YzU0OWM1YTRmYjY0YzZlMGUxZTBhOCIsCiAgInByb2ZpbGVOYW1" +
                                        "lIiA6ICJNSEZfU2tlbGV0b24iLAogICJzaWduYXR1cmVSZXF1aXJlZCIgOiB0cnVlLAogICJ" +
                                        "0ZXh0dXJlcyIgOiB7CiAgICAiU0tJTiIgOiB7CiAgICAgICJ1cmwiIDogImh0dHA6Ly90ZXh0dX" +
                                        "Jlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMjI3OTVjM2M2ZjM2ZDY3ZGVjZjlhMzE5NWUxMjgwNDB" +
                                        "iZWM1MjI2YjA1NWYyYjE2ZDQ2ZmExOWE5MTgwZTAyMyIKICAgIH0KICB9Cn0=")
                                .name(permissionUser.getHighestGroup().getColor() + nameResult.getName()).lore("§7Rang §8»§7 " + clanMember.getRole().display()).lore("§7Status §8»§c Offline").lore("§7Letzter Login §8» §b" + TimeFormat.format(playtimeUser.getLastLogin()))
                                .itemStack());
            } else {

                list.add(
                        new ItemBuilder(Material.SKULL_ITEM, 1, 3).headValue(skinProvider
                                        .getSkin(clanMember.getUuid())
                                        .getSkinValue())
                                .name(permissionUser.getHighestGroup().getColor() + nameResult.getName()).lore("§7Rang §8»§7 " + clanMember.getRole().display()).lore("§7Status §8»§7 Online auf: §e" + corePlayer.getServer())
                                .itemStack());
            }

        });
        return list;
    }

    @Getter
    @AllArgsConstructor
    public static class Friend {
        private final UUID uuid;
        private final boolean online;
    }

}
