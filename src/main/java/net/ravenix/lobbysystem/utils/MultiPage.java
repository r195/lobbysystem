package net.ravenix.lobbysystem.utils;

import net.ravenix.clansystem.bukkit.BukkitClan;
import net.ravenix.clansystem.shared.clan.Clan;
import net.ravenix.clansystem.shared.clan.IClanProvider;
import net.ravenix.core.bukkit.BukkitCore;
import net.ravenix.core.bukkit.util.ItemBuilder;
import net.ravenix.core.shared.skin.ISkin;
import net.ravenix.core.shared.skin.ISkinProvider;
import net.ravenix.friendsystem.bukkit.BukkitFriends;
import net.ravenix.friendsystem.shared.friend.IFriendProvider;
import net.ravenix.partysystem.bukkit.BukkitParty;
import net.ravenix.partysystem.shared.party.IPartyProvider;
import net.ravenix.partysystem.shared.party.Party;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;


public class MultiPage {

    public ArrayList<Inventory> pages = new ArrayList<Inventory>();
    public UUID id;
    public int currpage = 0;
    public static HashMap<UUID, MultiPage> users = new HashMap<UUID, MultiPage>();

    public MultiPage(ArrayList<ItemStack> items, String name, Player player) {
        this.id = UUID.randomUUID();
        Inventory page = getBlankPage(name, player);
        for (ItemStack item : items) {
            if (page.firstEmpty() == 35) {
                pages.add(page);
                page = getBlankPage(name, player);
                page.addItem(item);
            } else {
                page.addItem(item);
            }
        }
        pages.add(page);
        player.openInventory(pages.get(currpage));
        users.put(player.getUniqueId(), this);
    }



    private Inventory getBlankPage(String name, Player player) {
        Inventory page = Bukkit.createInventory(null, 6*9, name);

        page.setItem(44, new ItemBuilder(Material.ARROW).name("§bNächste Seite").itemStack());

        page.setItem(36, new ItemBuilder(Material.ARROW).name("§bLetzte Seite").itemStack());

        ISkinProvider skinProvider = BukkitCore.getInstance().getSkinProvider();
        ISkin skin = skinProvider.getSkin(player.getUniqueId());

        if (!name.equalsIgnoreCase("§3Replays")) {

            page.setItem(47, new ItemBuilder(Material.SKULL_ITEM, 1, 3).headValue(skin.getSkinValue()).name("§aFreunde").itemStack());
            page.setItem(48, new ItemBuilder(Material.PAPER, 1).name("§aFreundschaftsanfragen").itemStack());
            page.setItem(49, new ItemBuilder(Material.CAKE).name("§5Öffentliche Partys").itemStack());
            page.setItem(50, new ItemBuilder(Material.FIREWORK).name("§5Party").itemStack());
            page.setItem(51, new ItemBuilder(Material.IRON_CHESTPLATE).name("§6Clan").itemStack());

            if (name.equals("§aFreundschaftsanfragen")) {
                page.setItem(48, new ItemAPI(Material.PAPER).setName("§aFreundschaftsanfragen").addEnchant(Enchantment.KNOCKBACK, 1).setItemFlag(ItemFlag.HIDE_ENCHANTS).toItemStack());
                IFriendProvider friendProvider = BukkitFriends.getInstance().getFriendProvider();
                List<UUID> requests = friendProvider.getRequests(player.getUniqueId());
                if (requests == null) {
                    page.setItem(13, new ItemAPI(Material.BARRIER).setName("§cDu hast keine Freundschaftsanfragen").toItemStack());
                } else {
                    if (requests.size() == 0) {
                        page.setItem(13, new ItemAPI(Material.BARRIER).setName("§cDu hast keine Freundschaftsanfragen").toItemStack());
                    }
                }
            }

            if (name.equals("§5Öffentliche Partys")) {
                page.setItem(49, new ItemAPI(Material.CAKE).setName("§5Öffentliche Partys").addEnchant(Enchantment.KNOCKBACK, 1).setItemFlag(ItemFlag.HIDE_ENCHANTS).toItemStack());
                IPartyProvider partyProvider = BukkitParty.getInstance().getPartyProvider();
                List<Party> publicParties = partyProvider.getPublicParties();
                if (publicParties == null) {
                    page.setItem(13, new ItemAPI(Material.BARRIER).setName("§cEs gibt keine öffentlichen Partys").toItemStack());
                } else if (publicParties.size() == 0) {
                    page.setItem(13, new ItemAPI(Material.BARRIER).setName("§cEs gibt keine öffentlichen Partys").toItemStack());
                }
            }

            if (name.equals("§5Party")) {
                page.setItem(50, new ItemAPI(Material.FIREWORK).setName("§5Party").addEnchant(Enchantment.KNOCKBACK, 1).setItemFlag(ItemFlag.HIDE_ENCHANTS).toItemStack());
                IPartyProvider partyProvider = BukkitParty.getInstance().getPartyProvider();
                Party party = partyProvider.getParty(player.getUniqueId());
                if (party == null) {
                    page.setItem(13, new ItemAPI(Material.CAKE).setName("§aParty erstellen").toItemStack());
                }
            }

            if (name.equals("§6Clan")) {
                page.setItem(51, new ItemAPI(Material.IRON_CHESTPLATE).setName("§6Clan").addEnchant(Enchantment.KNOCKBACK, 1).setItemFlag(ItemFlag.HIDE_ENCHANTS).toItemStack());
                IClanProvider clanProvider = BukkitClan.getInstance().getClanProvider();
                Clan clan = clanProvider.getClan(player.getUniqueId());
                if (clan == null) {
                    page.setItem(13, new ItemAPI(Material.BARRIER).setName("§cDu bist in keinem Clan").toItemStack());
                }
            }

            if (name.equals("§aFreunde")) {
                IFriendProvider friendProvider = BukkitFriends.getInstance().getFriendProvider();
                List<UUID> friends = friendProvider.getFriends(player.getUniqueId());
                if (friends == null) {
                    page.setItem(13, new ItemAPI(Material.BARRIER).setName("§cDu hast keine Freunde").toItemStack());
                } else {
                    if (friends.size() == 0) {
                        page.setItem(13, new ItemAPI(Material.BARRIER).setName("§cDu hast keine Freunde").toItemStack());
                    }
                }
            }
        } else {
            page.setItem(47, new ItemBuilder(Material.STAINED_GLASS_PANE, 1,7).name("§c").itemStack());
            page.setItem(48, new ItemBuilder(Material.STAINED_GLASS_PANE, 1,7).name("§c").itemStack());
            page.setItem(49, new ItemBuilder(Material.EYE_OF_ENDER).name("§3Eigene Replays").enchanment(Enchantment.KNOCKBACK, 1).itemFlag(ItemFlag.HIDE_ENCHANTS).itemStack());
            page.setItem(50, new ItemBuilder(Material.STAINED_GLASS_PANE, 1,7).name("§c").itemStack());
            page.setItem(51, new ItemBuilder(Material.STAINED_GLASS_PANE, 1,7).name("§c").itemStack());
        }

        page.setItem(43, new ItemBuilder(Material.STAINED_GLASS_PANE, 1,7).name("§c").itemStack());
        page.setItem(42, new ItemBuilder(Material.STAINED_GLASS_PANE, 1,7).name("§c").itemStack());
        page.setItem(41, new ItemBuilder(Material.STAINED_GLASS_PANE, 1,7).name("§c").itemStack());
        page.setItem(40, new ItemBuilder(Material.STAINED_GLASS_PANE, 1,7).name("§c").itemStack());
        page.setItem(39, new ItemBuilder(Material.STAINED_GLASS_PANE, 1,7).name("§c").itemStack());
        page.setItem(38, new ItemBuilder(Material.STAINED_GLASS_PANE, 1,7).name("§c").itemStack());
        page.setItem(37, new ItemBuilder(Material.STAINED_GLASS_PANE, 1,7).name("§c").itemStack());

        page.setItem(45, new ItemBuilder(Material.STAINED_GLASS_PANE, 1,7).name("§c").itemStack());
        page.setItem(46, new ItemBuilder(Material.STAINED_GLASS_PANE, 1,7).name("§c").itemStack());
        page.setItem(52, new ItemBuilder(Material.STAINED_GLASS_PANE, 1,7).name("§c").itemStack());
        page.setItem(53, new ItemBuilder(Material.STAINED_GLASS_PANE, 1,7).name("§c").itemStack());

        return page;
    }

}
